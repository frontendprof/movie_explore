const root = document.querySelector(".autocomplete")


const fetchData = async (searchTerm) => {
    const res = await axios.get("http://www.omdbapi.com/", {
        params: {
            apikey: "1d8d8580",
            s: searchTerm
        }
    });

    if (res.data.Error) {
        return []
    }
    return res.data.Search;
}

root.innerHTML = `
        <label><b>Search for a movie</b></label>
        <input class="inputVal" />
        <div class="dropdown">
            <div class="dropdown-menu">
                <div class="dropdown-content results">      

                </div>
            </div>
        </div>
`;
const inputVal = document.querySelector('.inputVal');
const dropdown = document.querySelector(".dropdown")
const results = document.querySelector(".results");



const onInput = async (event) => {
    const movies = await fetchData(event.target.value)
    // console.log(movies);
    if (!movies.length) {
        dropdown.classList.remove("is-active");
        return
    }

    results.innerHTML = "";

    dropdown.classList.add("is-active");
    for (let mov of movies) {
        const option = document.createElement("a");
        const imgSrc = mov.Poster === "N/A" ? "" : mov.Poster;
        option.classList.add("dropdown-item");
        option.innerHTML = `
                <img src="${imgSrc}" />
                ${mov.Title}

            `
        option.addEventListener('click', () => {
            dropdown.classList.remove("is-active");
            inputVal.value = mov.Title;
            onMovieSelect(mov)
        })
        results.appendChild(option);
    }
}

inputVal.addEventListener("input", debounce(onInput, 500));
document.addEventListener("click", event => {
    if (!root.contains(event.target)) {
        dropdown.classList.remove("is-active")
    }
})

const onMovieSelect = async movie => {
    const res = await axios.get("http://www.omdbapi.com/", {
        params: {
            apikey: "1d8d8580",
            i: movie.imdbID
        }
    })
    document.querySelector("#summary").innerHTML = movieTemplate(res.data)
}

const movieTemplate = (movDetail) => {
    return `
        <article class="media">
            <figure class="media-left">
                <p class="image">
                    <img src=${movDetail.Poster}>
                </p>
            </figure>
            <div class="media-content">
                <div class="content">
                    <h1>${movDetail.Title}</h1>
                    <h1>${movDetail.Genre}</h1>
                    <h1>${movDetail.Plot}</h1>

                </div>
            </div>
        </article>

        <article class="notification is-primary">
            <p class="title">${movDetail.Awards}</p>
            <p class="subtitle">Awards</p>
        </article>
    
        <article class="notification is-primary">
            <p class="title">${movDetail.BoxOffice}</p>
            <p class="subtitle">Box Office</p>
        </article>
        
        <article class="notification is-primary">
            <p class="title">${movDetail.Metascore}</p>
            <p class="subtitle">Metascore</p>
        </article>
        
        <article class="notification is-primary">
            <p class="title">${movDetail.imdbRating}</p>
            <p class="subtitle">IMDB Rating</p>
        </article>
        
        <article class="notification is-primary">
            <p class="title">${movDetail.imdbVotes}</p>
            <p class="subtitle">IMDB Votes</p>
        </article>
    `
}