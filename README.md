# Movie fight (comparison) app.
1. Make an api call to omdb server via axios library;(http://www.omdbapi.com/)(1d8d8580)
2. Passing input value as api call function argument.
3. Debounce an input
4. Implementing reusable debounce
5. Moving HTML Generation
6. Clearing up existing list
7. Handling broken images error(simba for example)
8. Closing drowdown menu and handling empty input and movie selection
9. Making a followup request with new function(onMovieSelect)
10. Render detailed data about selected movie, (movieTemplate)
